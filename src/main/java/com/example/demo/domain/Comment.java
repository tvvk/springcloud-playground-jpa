package com.example.demo.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
@GenericGenerator(name="commentIdGenerator", strategy = "com.example.demo.repository.ToString2IdGenerator")	
public class Comment {
	
	@Id
	@GeneratedValue(generator="commentIdGenerator")
	private String id;
	
	private Date date;
	private String text;
	
	protected Comment() {
	}
	
	public Comment(Date date, String text) {
		this.date = date;
		this.text = text;
	}
	
	public Date getDate() {
		return date;
	}
	
	public String getText() {
		return text;
	}
	
	@Override
	public String toString() {
		return text + " (" + date + ")";
	}
}
