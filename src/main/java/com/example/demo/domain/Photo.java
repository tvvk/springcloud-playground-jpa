package com.example.demo.domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Photo {
	
	@Id
	@GeneratedValue
	private long id;
	
	private String title;
	private Rectangle dimension;
	
	@OneToMany(cascade=CascadeType.ALL) // Comments müssen nicht erst gespeichert werden
	@JoinColumn(name="photo") // "photo" referenziert die Spalte in der "comment" Tabelle. Dafür wird keine m-n Tabelle angelegt.
	private Collection<Comment> comments;
	
	protected Photo() {
	}
	
	public Photo(String title, Rectangle dimension) {
		this.title = title;
		this.dimension = dimension;
	}
	
	public String getTitle() {
		return title;
	}
	
	public Rectangle getDimension() {
		return dimension;
	}
	
	public Collection<Comment> getComments() {
		return comments;
	}
	
	public void addComment(String comment) {
		this.comments.add(new Comment(new Date(), comment));
	}
	
	@Override
	public String toString() {
		return title + " [" + dimension + "]";
	}
}
