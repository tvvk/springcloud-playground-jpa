package com.example.demo.domain;

import javax.persistence.Embeddable;

@Embeddable
public class Rectangle {

	private int width;
	private int height;
	
	protected Rectangle() {
	}
	
	public Rectangle(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	@Override
	public String toString() {
		return width + "x" + height;
	}
}
