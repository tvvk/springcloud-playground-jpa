package com.example.demo.repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.example.demo.domain.Photo;

public class GoodPhotoSpecification implements Specification<Photo> {

	private static final long serialVersionUID = 5851747797193930177L;

	private static final int MIN_DIMENSION = 250;
	private static final int MIN_NUMBER_OF_COMMENTS = 2;

	@Override
	public Predicate toPredicate(Root<Photo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		return criteriaBuilder.and(
				criteriaBuilder.ge(criteriaBuilder.size(root.get("comments")), MIN_NUMBER_OF_COMMENTS),
				criteriaBuilder.ge(root.get("dimensionWidth"), MIN_DIMENSION),
				criteriaBuilder.ge(root.get("dimensionHeight"), MIN_DIMENSION));
	}

}
