package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.example.demo.domain.Photo;

public interface PhotoRepository extends JpaRepository<Photo, Long>, JpaSpecificationExecutor<Photo> {
	
	public List<Photo> findByDimensionWidthBetween(int min, int max);
	
	public List<Photo> findByDimensionHeightBetween(int min, int max);
}
