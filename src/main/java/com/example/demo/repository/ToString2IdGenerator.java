package com.example.demo.repository;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLong;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

public class ToString2IdGenerator implements IdentifierGenerator {
	
	private static final AtomicLong idGenerator = new AtomicLong(System.currentTimeMillis());
	
	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
		return "" + idGenerator.incrementAndGet() + " - " + object;
	}

}
