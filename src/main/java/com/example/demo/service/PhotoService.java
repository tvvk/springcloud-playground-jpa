package com.example.demo.service;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.example.demo.domain.Photo;
import com.example.demo.domain.Rectangle;
import com.example.demo.repository.GoodPhotoSpecification;
import com.example.demo.repository.PhotoRepository;

@Service
public class PhotoService {

	@Autowired
	private PhotoRepository repository;
	
	private final Random random = new Random();
	
	public Photo createPhoto(String title, int width, int height) {
		Photo photo = new Photo(title, new Rectangle(width, height));
		return repository.save(photo);
	}

	public List<Photo> getAll(Integer page) {
		return page != null ? repository.findAll(PageRequest.of(page, 10)).getContent() : repository.findAll();
	}

	public List<Photo> getByWidth(int minWidth, int maxWidth) {
		return repository.findByDimensionWidthBetween(minWidth, maxWidth);
	}

	public List<Photo> getByHeight(int minHeight, int maxHeight) {
		return repository.findByDimensionHeightBetween(minHeight, maxHeight);
	}
	
	public List<Photo> getGoodPhotos() {
		return repository.findAll(new GoodPhotoSpecification());
	}

	public void generateDemoPhotos(int amount) {
		repository.saveAll(IntStream.range(0, amount)
				.mapToObj(i -> new Photo("Demo Photo " + i, new Rectangle(random.nextInt(2500), random.nextInt(2500))))
				.collect(Collectors.toList()));
	}
	
	@Transactional
	public void comment(long photoId, String text) {
		repository.findById(photoId).ifPresent(photo -> {
			photo.addComment(text);	
			repository.save(photo);
		});
	}
	
}
