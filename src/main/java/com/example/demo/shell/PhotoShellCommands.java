package com.example.demo.shell;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import com.example.demo.domain.Photo;
import com.example.demo.service.PhotoService;

@ShellComponent
public class PhotoShellCommands {
	
	@Autowired
	private PhotoService service;

	@ShellMethod("Lists all photos")
	public List<Photo> listAll(@ShellOption(defaultValue = ShellOption.NULL) Integer page) {
		return service.getAll(page);
	}
	
	@ShellMethod("Generates some demo photos")
	public void generate(@ShellOption int amount) {
		service.generateDemoPhotos(amount);
	}
	
	@ShellMethod("Comment a photo")
	public void comment(@ShellOption int id, @ShellOption String text) {
		service.comment(id, text);
	}
	
	@ShellMethod("Find photos by width")
	public List<Photo> findByWidth(@ShellOption int minWidth, @ShellOption int maxWidth) {
		return service.getByWidth(minWidth, maxWidth);
	}
	
	@ShellMethod("Find good photos")
	public List<Photo> findGoodPhotos() {
		return service.getGoodPhotos();
	}
}
